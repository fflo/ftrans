package ftrans

import (
	"fmt"
)

type SerTransition struct {
	Target uint64
	Output string
	Char   rune
}

func mustFindStateId(s *state, states map[*state]uint64) uint64 {
	if s == nil {
		return 0
	} else if id, ok := states[s]; ok {
		return id
	}
	panic(fmt.Sprintf("cannot find target state %p", s))
}

func newSerTransitions(ts transitions, states map[*state]uint64) []SerTransition {
	res := make([]SerTransition, len(ts), len(ts))
	for i, t := range ts {
		res[i] = SerTransition{
			Target: mustFindStateId(t.target, states),
			Output: string(t.output),
			Char:   t.char,
		}
	}
	return res
}

type SerState struct {
	Foutput     string
	Id, Ftarget uint64
	Ts          []SerTransition
}

func newSerState(s *state, states map[*state]uint64) SerState {
	return SerState{
		Foutput: string(s.f.output),
		Id:      mustFindStateId(s, states),
		Ftarget: mustFindStateId(s.f.target, states),
		Ts:      newSerTransitions(s.ts, states),
	}
}

type SerTransducer map[uint64]SerState

// GetSerialization returns a SerTransducer that can be serialized.
func (ft Transducer) GetSerialization() SerTransducer {
	states := make(map[*state]uint64)
	var id uint64
	ft.Walk(func(link Link) bool {
		if _, ok := states[link.Target]; !ok {
			id++
			states[link.Target] = id
		}
		return true
	})
	ser := make(SerTransducer)
	for state, id := range states {
		ser[id] = newSerState(state, states)
	}
	return ser
}

// NewTransducerFromSerialization creates a new transducer from a SerTransducer.
func NewTransducerFromSerialization(ser SerTransducer) Transducer {
	states := make(map[uint64]*state)
	states[0] = nil
	for id, _ := range ser {
		states[id] = &state{}
	}
	for id, s := range ser {
		states[id].f.output = []byte(s.Foutput)
		states[id].f.target = states[s.Ftarget]
		states[id].ts = make([]transition, len(s.Ts), len(s.Ts))
		for i, t := range s.Ts {
			states[id].ts[i].output = []byte(t.Output)
			states[id].ts[i].target = states[t.Target]
			states[id].ts[i].char = t.Char
		}
	}
	return Transducer{states[1]}
}
