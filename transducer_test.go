package ftrans

import (
	"bytes"
	"fmt"
	"sort"
	"testing"
)

var (
	chars = [...]rune{
		'𐀬', '𐌈', '世', '界', 'A', 'B', 'C', '1', '2', '3',
		'ꦪ', 'x', 'ſ', 'ß', 'ä', 'æ', 'a', 'b', 'c', 'ᚠ',
	}
)

func makeState() *state {
	s := &state{}
	for _, char := range chars {
		s.ts = append(s.ts, transition{
			target: nil, output: nil, char: char})
	}
	return s
}

func TestStateSort(t *testing.T) {
	s := makeState()
	if sort.IsSorted(s.ts) {
		t.Errorf("transitions are sorted")
	}
	s.sort()
	if !sort.IsSorted(s.ts) {
		t.Errorf("transitions are not sorted")
	}
}

func TestStateSearchPositive(t *testing.T) {
	s := makeState()
	s.sort()
	for _, char := range chars {
		if _, ok := s.search(char); !ok {
			t.Errorf("could not find transition for %c", char)
		}
	}
}

func TestStateSearchNegative(t *testing.T) {
	s := makeState()
	s.sort()
	for _, char := range []rune{'M', 'F', '↔', '.', ',', '→', '„'} {
		if _, ok := s.search(char); ok {
			t.Errorf("found transition for %c", char)
		}
	}
}

func TestDeltaWithNilState(t *testing.T) {
	teststr := "abcd→ßäÖ↔"
	var s *state = nil
	buffer := &bytes.Buffer{}
	for _, char := range teststr {
		s.Delta(char, buffer)
	}
	if buffer.String() != teststr {
		t.Errorf("Delta: %q != %q", buffer.String(), teststr)
	}
}

func TestDownWithNilState(t *testing.T) {
	var s *state = nil
	buffer := &bytes.Buffer{}
	err := s.Down(buffer)
	if err != nil {
		t.Errorf("Down: %s", err.Error())
	}
	if buffer.String() != "" {
		t.Errorf("Down: %s != ", buffer.String())
	}
}

func makeFailureDownLink(str string) *state {
	init := &state{}
	s := init
	for _, char := range str {
		tmp := &state{f: failureLink{
			target: s, output: rune2bytes(char)}}
		s = tmp
	}
	return s
}

func TestDownState(t *testing.T) {
	for _, test := range []struct{ test, want string }{
		{"abc", "cba"},
		{"abcdef", "fedcba"},
		{"DEADBEEF", "FEEBDAED"},
		{"Hello", "olleH"},
		{"世界", "界世"},
	} {
		buffer := &bytes.Buffer{}
		s := makeFailureDownLink(test.test)
		if err := s.Down(buffer); err != nil {
			t.Errorf("Down: %s", err.Error())
		}
		if buffer.String() != test.want {
			t.Errorf("Down: %q != %q", buffer.String(), test.want)
		}
	}
}

func makeConnectedState(char rune, output, failure string) *state {
	s := &state{}
	s.ts = append(s.ts, transition{
		target: &state{},
		output: []byte(output),
		char:   char,
	})
	s.f = failureLink{output: []byte(failure), target: &state{}}
	s.sort()
	return s
}

func TestDeltaState(t *testing.T) {
	for _, test := range []struct {
		good, bad    rune
		output, fail string
	}{
		{'a', 'X', "a", "fail"},
		{'Y', 'X', "Y", "fail"},
		{'ä', 'X', "ä", "fail"},
		{'1', 'X', "1", "fail"},
		{'Ⅴ', 'X', "Ⅴ", "fail"},
	} {
		s := makeConnectedState(test.good, test.output, test.fail)
		buffer := &bytes.Buffer{}
		if _, err := s.Delta(test.good, buffer); err != nil {
			t.Errorf("Delta: %s", err.Error())
		}
		if buffer.String() != test.output {
			t.Errorf("Delta: %q != %q",
				buffer.String(), test.output)
		}
		buffer.Reset()
		if _, err := s.Delta(test.bad, buffer); err != nil {
			t.Errorf("Delta: %s", err.Error())
		}
		if buffer.String() != test.fail+fmt.Sprintf("%c", test.bad) {
			t.Errorf("Delta: %q != %q",
				buffer.String(), test.fail)
		}
	}
}

func TestEmptyTransducerRewrite(t *testing.T) {
	for _, test := range []string{"abcde", "Ärger", "世界", "ᚠᛚᚬᚱᛁᚨᚾ"} {
		var ft Transducer
		buffer := &bytes.Buffer{}
		if _, err := ft.Rewrite(ft.Initial(), test, buffer); err != nil {
			t.Errorf("Rewrite: %s", err.Error())
		}
		if buffer.String() != test {
			t.Errorf("Rewrite [%s]: %q != %q",
				test, buffer.String(), test)
		}
	}
}

func TestEmptyTransducerDown(t *testing.T) {
	var ft Transducer
	buffer := &bytes.Buffer{}
	if err := ft.Down(ft.Initial(), buffer); err != nil {
		t.Errorf("Down: %s", err.Error())
	}
	if buffer.String() != "" {
		t.Errorf("Down: %q != %q", buffer.String(), "")
	}
}
