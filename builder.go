package ftrans

import (
	"fmt"
	"unicode/utf8"
)

// type Builder is a builder that can be used to build failure transducers.
type Builder struct {
	ft      Transducer
	output  map[*state][]byte
	initial *state
}

// NewBuilder returns a new Builder
func NewBuilder() *Builder {
	return &Builder{
		ft:     Transducer{},
		output: make(map[*state][]byte),
	}
}

// Add adds a new rewrite rule to the resulting failure transducer.
func (b *Builder) Add(key, val string) {
	// fmt.Printf("[b.Add(%s,%s)] init: %v\n", key, val, b.ft.initial)
	if b.initial == nil {
		b.initial = &state{}
		b.ft.initial = b.initial
	}
	// fmt.Printf("[b.Add(%s,%s)] init: %v\n", key, val, b.ft.initial)
	s := b.ft.initial
	for _, char := range key {
		tmp := findTargetState(s, char)
		if tmp == nil {
			tmp = &state{}
			s.ts = append(s.ts, transition{
				target: tmp,
				char:   char,
				output: nil,
			})
		}
		s = tmp
	}
	b.output[s] = []byte(val)
}

// Build creates a failure transducer.
func (b *Builder) Build() Transducer {
	// fmt.Printf("[b.Build()]\n")
	// fmt.Printf("[b.Build()] ft: %v\n", b.ft)
	if b.ft.initial != nil {
		// fmt.Printf("walkBreadthFirst()\n")
		walkBreadthFirst(b.ft.initial, b.handleLink)
	}
	// fmt.Printf("[b.Build()] ft: %v\n", b.ft)
	return b.ft
}

func (b *Builder) Reset() {
	b.ft = Transducer{}
}

type link struct {
	p *state
	t transition
	q *state
	l int
}

func (b *Builder) handleLink(link link) {
	// fmt.Printf("[handleLink(%v)]\n", link)
	checkLink(link)
	link.q.sort()    // q is never null
	if link.l == 0 { // initial state's failure is a the nil state
		// fmt.Printf("[handleLink(%v)] initial\n", link)
		link.q.f.output = nil
		link.q.f.target = nil
	} else if link.l == 1 { // first level
		// fmt.Printf("[handleLink(%v)] first level\n", link)
		output := b.getOutput(link.t, link.q)
		link.q.f.target = link.p
		link.q.f.output = output
	} else if output, ok := b.output[link.q]; ok {
		// fmt.Printf("[handleLink(%v)] link to initial\n", link)
		link.q.f.target = b.initial
		link.q.f.output = output
	} else {
		// fmt.Printf("[handleLink(%v)] THE WALK\n", link)
		p_ := link.p.f.target
		// r, output := theWalk(p_, link.t.char)
		r, output := theWalk(p_, link.p.f.output, link.t.char)
		link.q.f.target = r
		link.q.f.output = output
	}
}

func (b *Builder) getOutput(t transition, s *state) []byte {
	if output, ok := b.output[s]; ok {
		return output
	} else {
		return rune2bytes(t.char)
	}
}

func rune2bytes(char rune) []byte {
	n := utf8.RuneLen(char)
	buf := make([]byte, n)
	utf8.EncodeRune(buf, char)
	return buf
}

// func theWalk(s *state, c rune) (*state, []byte) {
func theWalk(s *state, output []byte, c rune) (*state, []byte) {
	// fmt.Printf("[theWalk(%p, %c)]\n", s, c)
	// var output []byte = nil
	for {
		if t, ok := s.search(c); ok {
			// fmt.Printf("[theWalk(%p, %c)] found: %p\n", s, c, t.target)
			return t.target, output
		}
		// fmt.Printf("[theWalk(%p, %c)] output: %q\n", s, c, output)
		output = append(output, s.f.output...)
		if s.f.target == nil {
			break
		}
		s = s.f.target
	}
	// fmt.Printf("[theWalk(%p, %c)] to initial state: %p\n", s, c, s)
	return s, output
}

func checkLink(link link) {
	if link.q == nil || link.l < 0 {
		panic(fmt.Sprintf(
			"[checkLink] invalid link encunterd: %v", link))
	}
}

func walkBreadthFirst(s *state, f func(link link)) {
	// fmt.Printf("[walkBreadthFirst(%p,%p)]\n", s, f)
	queue := []link{link{p: nil, t: transition{}, q: s, l: 0}}
	for len(queue) > 0 {
		current := queue[0]
		// fmt.Printf("[walkBreadthFirst(%p,%p)] current: %v\n", s, f, current)
		checkLink(current)
		queue = queue[1:]
		for _, t := range current.q.ts {
			// fmt.Printf("[walkBreadthFirst(%p,%p)] transition: %v\n", s, f, t)
			queue = append(queue, link{
				p: current.q,
				t: t,
				q: t.target,
				l: current.l + 1,
			})
		}
		// fmt.Printf("[walkBreadthFirst(%p,%p)] current: %v\n", s, f, current)
		f(current)
	}
}

func findTargetState(state *state, char rune) *state {
	for _, t := range state.ts {
		if t.char == char {
			return t.target
		}
	}
	return nil
}
