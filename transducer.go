package ftrans

import (
	"bufio"
	"io"
	"sort"
	"unicode/utf8"
)

type transition struct {
	target *state
	output []byte
	char   rune
}

// Char returns the rune of a transition
func (t transition) Char() rune {
	return t.char
}

// Output returns the output of a transition.
func (t transition) Output() []byte {
	return t.output
}

// Target returns the target of a transition.
func (t transition) Target() *state {
	return t.target
}

// func (t *transition) GobEncode() ([]byte, error) {
// 	buf := &bytes.Buffer{}
// 	e := gob.NewEncoder(buf)
// 	if err := e.Encode(t.target); err != nil {
// 		return nil, err
// 	}
// 	if err := e.Encode(t.output); err != nil {
// 		return nil, err
// 	}
// 	if err := e.Encode(t.char); err != nil {
// 		return nil, err
// 	}
// 	return buf.Bytes(), nil
// }

type failureLink struct {
	target *state
	output []byte
}

type transitions []transition

func (ts transitions) Len() int           { return len(ts) }
func (ts transitions) Swap(i, j int)      { ts[i], ts[j] = ts[j], ts[i] }
func (ts transitions) Less(i, j int) bool { return ts[i].char < ts[j].char }

type state struct {
	f  failureLink
	ts transitions
}

// Down follows all failure links down to the initial state of the transducer.
// All encountered output is written into the writer.
func (s *state) Down(out io.Writer) error {
	if s == nil {
		return nil
	}
	for s.f.target != nil {
		if _, err := out.Write(s.f.output); err != nil {
			return err
		}
		s = s.f.target
	}
	return nil
}

// Delta makes one transition from a state with the given rune.
// The encountered output is written into the writer.
func (s *state) Delta(c rune, out io.Writer) (*state, error) {
	if s == nil {
		err := writeRune(c, out)
		return nil, err
	} else if t, ok := s.search(c); ok {
		_, err := out.Write(t.output)
		return t.target, err
	} else if s.f.target != nil {
		if _, err := out.Write(s.f.output); err != nil {
			return nil, err
		}
		return s.f.target.Delta(c, out)
	} else {
		err := writeRune(c, out)
		return s, err
	}
}

func writeRune(c rune, out io.Writer) error {
	var buf [utf8.UTFMax]byte
	n := utf8.EncodeRune(buf[:], c)
	_, err := out.Write(buf[0:n])
	return err
}

// FailureTarget returns the target of a state's failure link.
func (s *state) FailureTarget() *state {
	if s == nil {
		return nil
	}
	return s.f.target
}

// FailureOutput returns the output of a state's failure link.
func (s *state) FailureOutput() []byte {
	if s == nil {
		return nil
	}
	return s.f.output
}

// IsInitialState returns true iff the state is the
// initial state of the transducer.
func (s *state) IsInitialState() bool {
	if s != nil {
		return s.f.target == nil
	}
	return false
}

func (s *state) search(c rune) (transition, bool) {
	if i := sort.Search(s.ts.Len(), func(i int) bool {
		return s.ts[i].char >= c
	}); i < s.ts.Len() && s.ts[i].char == c {
		return s.ts[i], true
	} else {
		return transition{}, false
	}
}

func (s *state) sort() {
	sort.Sort(s.ts)
}

// func (s *state) GobEncode() ([]byte, error) {
// 	if s != nil {
// 		buf := &bytes.Buffer{}
// 		e := gob.NewEncoder(buf)
// 		if s.f.target != nil {
// 			if err := e.Encode(s.f.target); err != nil {
// 				return nil, err
// 			}
// 		}
// 		if err := e.Encode(s.f.output); err != nil {
// 			return nil, err
// 		}
// 		if err := e.Encode(s.ts); err != nil {
// 			return nil, err
// 		}
// 		return buf.Bytes(), nil
// 	} else {
// 		return nil, nil
// 	}
// }

// Transducer with failure links. It holds an initial state.
// An empty Transducer simply copies the input to the ouptput.
type Transducer struct {
	initial *state
}

// Initial returns the initial state of a Transducer.
func (ft Transducer) Initial() *state {
	return ft.initial
}

// Rewrite rewrites a string with a transducer.
func (ft Transducer) Rewrite(s *state, str string, out io.Writer) (*state, error) {
	for _, c := range str {
		tmp, err := s.Delta(c, out)
		if err != nil {
			return nil, err
		}
		s = tmp
	}
	return s, nil
}

// RewriteAll reads input from a reader and rewrites it to a writer.
// It uses delim to as separator for read chunks.
func (ft Transducer) RewriteAll(in io.Reader, out io.Writer, delim byte) error {
	reader := bufio.NewReader(in)
	s := ft.initial
	for {
		line, err1 := reader.ReadString(delim)
		if err1 == nil || err1 == io.EOF {
			tmp, err2 := ft.Rewrite(s, line, out)
			if err2 != nil {
				return err2
			}
			s = tmp
			if err1 == io.EOF {
				break
			}
		} else {
			return err1
		}
	}
	return ft.Down(s, out)
}

// Down follows the failure links down to the initial state of the Transducer.
func (ft Transducer) Down(s *state, out io.Writer) error {
	return s.Down(out)
}

// Walk iterates over the transducer in depth first order.
func (ft Transducer) Walk(f func(link Link) bool) {
	doWalk(ft.initial, f)
}

func doWalk(s *state, f func(link Link) bool) {
	if s == nil {
		return
	}
	stack := []Link{Link{Source: nil, Transition: transition{}, Target: s}}
	for len(stack) > 0 {
		last := len(stack) - 1
		current := stack[last]
		stack = stack[0:last]
		for _, t := range current.Target.ts {
			stack = append(stack, Link{
				Source:     current.Target,
				Transition: t,
				Target:     t.target,
			})
		}
		// If f returns false, stop.
		if f(current) == false {
			return
		}
	}
}

// Link is a triple that represents a transition in the transducer.
// It contains the source and target states and the transition between them.
type Link struct {
	Source, Target *state
	Transition     transition
}

// IsInitial returns true iff the Link represents the initial state of
// the transducer.
func (l Link) IsInitial() bool {
	return l.Source == nil
}
