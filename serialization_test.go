package ftrans

import (
	"testing"
)

func TestSerializeEmptyTransducer(t *testing.T) {
	var ft Transducer
	ser := ft.GetSerialization()
	if len(ser) != 0 {
		t.Errorf("Len: %d != %d", len(ser), 0)
	}
}

func TestSerializeTransducerWithSingleEmptyState(t *testing.T) {
	ft := Transducer{&state{}}
	ser := ft.GetSerialization()
	if len(ser) != 1 {
		t.Errorf("len: %d != %d", len(ser), 1)
	}
	if _, ok := ser[1]; !ok {
		t.Errorf("ser: could not find initial state")
	}
}

func TestSerializeSeriousTransducer(t *testing.T) {
	ft := makeRewriter([]keyval{
		{"a", "1"},
		{"ab", "2"},
		{"abcc", "3"},
		{"babc", "44"},
		{"c", "55"},
	})
	ser := ft.GetSerialization()
	if len(ser) != 10 {
		t.Errorf("len: %d != %d", len(ser), 10)
	}
	for i := 1; i <= 10; i++ {
		if _, ok := ser[uint64(i)]; !ok {
			t.Errorf("ser: could not find state %d", i)
		}
	}
}

func TestDeserializeEmptyTransducer(t *testing.T) {
	ft := NewTransducerFromSerialization(Transducer{}.GetSerialization())
	if ft.initial != nil {
		t.Errorf("deser: expected empty transducer")
	}
}

func TestDeserializeTransducerWithSingleEmptyState(t *testing.T) {
	ft := NewTransducerFromSerialization(
		Transducer{&state{}}.GetSerialization())
	if ft.initial == nil {
		t.Errorf("deser: expected non empty transducer")
	}
}

func TestDeserializeSeriousTransducer(t *testing.T) {
	ft := NewTransducerFromSerialization(
		makeRewriter([]keyval{
			{"a", "1"},
			{"ab", "2"},
			{"abcc", "3"},
			{"babc", "44"},
			{"c", "55"},
		}).GetSerialization())
	if ft.initial == nil {
		t.Errorf("deser: expected non empty transducer")
	}
	var n int
	ft.Walk(func(Link) bool {
		n++
		return true
	})
	if n != 10 {
		t.Errorf("deser: %d != %d", n, 10)
	}
}
