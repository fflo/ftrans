package ftrans

import (
	"bytes"
	// "fmt"
	"testing"
)

func TestBuildEmptyTransducer(t *testing.T) {
	for _, test := range []struct{ test, want string }{
		{"abcde", "abcde"},
		{"", ""},
	} {
		builder := NewBuilder()
		ft := builder.Build()
		buffer := &bytes.Buffer{}
		ft.Rewrite(ft.Initial(), test.test, buffer)
		if test.want != buffer.String() {
			t.Errorf("Rewrite [%s]: %q != %q",
				test.test, test.want, buffer.String())
		}
	}
}

type keyval struct {
	key, val string
}

func makeRewriter(keyvals []keyval) Transducer {
	builder := NewBuilder()
	for _, test := range keyvals {
		builder.Add(test.key, test.val)
	}
	return builder.Build()
}

func TestAddToBuilder(t *testing.T) {
	ft := makeRewriter([]keyval{{"a", "b"}})
	if ft.initial == nil {
		t.Errorf("Add: Empty Transducer")
	}
}

func TestSimpleRewrite(t *testing.T) {
	ft := makeRewriter([]keyval{
		{"a", "1"},
		{"ab", "2"},
		{"abcc", "3"},
		{"babc", "44"},
		{"c", "55"},
	})
	testRewrite(t, ft, "abcbbbabccb", "255bb4455b")
}

// func TestToDot(t *testing.T) {
// 	ft := makeRewriter([]keyval{
// 		{"a", "1"},
// 		{"ab", "2"},
// 		{"abcc", "3"},
// 		{"babc", "44"},
// 		{"c", "55"},
// 	})
// 	fmt.Printf("digraph { // DOTCODE\n")
// 	fmt.Printf("rankdir=LR; //DOTCODE\n")
// 	ft.Walk(printDot)
// 	fmt.Printf("} // DOTCODE\n")
// }
//
// func printDot(link Link) bool {
// 	if link.IsInitial() {
// 		fmt.Printf("0 [style=invisible,label=\"\",width=0,height=0] // DOTCODE\n")
// 		fmt.Printf("0 -> \"%p\" // DOTCODE\n", link.Target)
// 		fmt.Printf("\"%p\" [peripheries=2] // DOTCODE\n", link.Target)
// 		fmt.Printf("\"%p\" -> \"%p\" [label=\"?:?\"] // DOTCODE\n",
// 			link.Target, link.Target)
// 	} else {
// 		fmt.Printf("\"%p\" [label=\"\"] // DOTCODE\n", link.Source)
// 		fmt.Printf("\"%p\" [label=\"\"] // DOTCODE\n", link.Target)
// 		fmt.Printf("\"%p\" -> \"%p\" [label=%q] // DOTCODE\n",
// 			link.Source, link.Target,
// 			fmt.Sprintf("%c:%s", link.Transition.char,
// 				link.Transition.output))
// 		fmt.Printf("\"%p\" -> \"%p\" [label=%q style=dashed] // DOTCODE\n",
// 			link.Target, link.Target.f.target,
// 			string(link.Target.f.output))
// 	}
// 	return true
// }

func testRewrite(t *testing.T, ft Transducer, test, want string) {
	buffer := &bytes.Buffer{}
	s, err := ft.Rewrite(ft.Initial(), test, buffer)
	if err != nil {
		t.Errorf("Rewrite: %s", err.Error())
	}
	if err := ft.Down(s, buffer); err != nil {
		t.Errorf("Down: %s", err.Error())
	}
	if buffer.String() != want {
		t.Errorf("Rewrite [%s]: %q != %q", test, buffer.String(), want)
	}
}

func TestRewrite(t *testing.T) {
	for _, test := range []struct{ test, want string }{
		{"test", "TEST"},
		{"über", "over"},
		{"xyz", "XYZ"},
		{"Hello", "世界"},
		{"a test", "a TEST"},
		{"a über", "a over"},
		{"a xyz", "a XYZ"},
		{"a Hello", "a 世界"},
		{"test b", "TEST b"},
		{"über b", "over b"},
		{"xyz b", "XYZ b"},
		{"a test b", "a TEST b"},
		{"a über b", "a over b"},
		{"a xyz b", "a XYZ b"},
		{"a Hello", "a 世界"},
		{"Hello this is a test", "世界 this is a TEST"},
		{"Hello this is über", "世界 this is over"},
		{"über test", "over TEST"},
		{"this is a test", "this is a TEST"},
	} {
		ft := makeRewriter([]keyval{
			{"test", "TEST"},
			{"über", "over"},
			{"xyz", "XYZ"},
			{"Hello", "世界"},
		})
		testRewrite(t, ft, test.test, test.want)
	}
}
